package com.hxh.design_pattern.structure.proxy;

import android.util.Log;

import com.hxh.design_pattern.service.OrderService;
import com.hxh.design_pattern.service.impl.OutOrderServiceImpl;

public class ProxyOrder implements OrderService {
    OrderService orderService = new OutOrderServiceImpl();
    @Override
    public int saveOrder() {
        Log.d("保存订单","开始海外下订单");
        return orderService.saveOrder();
    }
}
