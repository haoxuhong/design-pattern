package com.hxh.design_pattern.action.strategy;

import android.util.Log;

public class RedPen implements Strategy {
    @Override
    public void draw(int radius, int x, int y) {
        Log.d("画图","用红色笔画图,radius:" + radius + ", x:" + x + ", y:" + y);
    }
}
