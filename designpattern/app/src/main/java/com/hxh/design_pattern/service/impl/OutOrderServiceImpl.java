package com.hxh.design_pattern.service.impl;


import android.util.Log;

import com.hxh.design_pattern.service.OrderService;

/**
 * 海外订单
 */
public class OutOrderServiceImpl implements OrderService {
    @Override
    public int saveOrder() {
        Log.d("保存订单","下单成功，订单号： 66666666");

        return 66666666;
    }
}
