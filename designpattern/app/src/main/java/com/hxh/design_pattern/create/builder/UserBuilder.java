package com.hxh.design_pattern.create.builder;

public class UserBuilder {
    private String name;
    private String nickName;
    private String sex;
    private int age;

    public UserBuilder() {

    }

    public UserBuilder name(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder nickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public UserBuilder sex(String sex) {
        this.sex = sex;
        return this;
    }

    public UserBuilder age(int age) {
        this.age = age;
        return this;
    }

    public User build() {
        if (name == null || nickName == null) {
            throw new RuntimeException("用户名和昵称必填");
        }
        if (age <= 0 || age >= 150) {
            throw new RuntimeException("年龄不合法");
        }
        // 还可以做赋予”默认值“的功能
        if (nickName == null) {
            nickName = name;
        }
        return new User(name, nickName, sex, age);
    }
}
