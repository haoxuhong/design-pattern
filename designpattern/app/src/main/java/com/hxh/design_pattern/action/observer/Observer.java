package com.hxh.design_pattern.action.observer;

public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
