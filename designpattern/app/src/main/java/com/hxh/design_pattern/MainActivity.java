package com.hxh.design_pattern;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.hxh.design_pattern.action.observer.BinaryObserver;
import com.hxh.design_pattern.action.observer.HexObserver;
import com.hxh.design_pattern.action.observer.Subject;
import com.hxh.design_pattern.action.strategy.RedPen;
import com.hxh.design_pattern.action.strategy.StrategyClient;
import com.hxh.design_pattern.action.template.ConcreteTemplate;
import com.hxh.design_pattern.bean.bag.AppleBag2;
import com.hxh.design_pattern.bean.bag.OrangeBag2;
import com.hxh.design_pattern.create.builder.User;
import com.hxh.design_pattern.create.builder.UserBuilder;
import com.hxh.design_pattern.create.factory.factorymethod.AppleBagFactory;
import com.hxh.design_pattern.create.factory.factorymethod.BagFactory;
import com.hxh.design_pattern.structure.adapter.OrangeBag2Adapter;
import com.hxh.design_pattern.structure.proxy.ProxyOrder;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //建造者模式
//        User user = User.builder().age(10).name("hao").nickName("HAOXU").build();


        //工厂模式
       /* BagFactory bagFactory =new  AppleBagFactory();
        bagFactory.getBag("A");*/

       //代理模式
        /*ProxyOrder proxyOrder = new ProxyOrder();
        proxyOrder.saveOrder();*/

        //适配器模式
        /*AppleBag2 appleBag = new AppleBag2();
        OrangeBag2 orangeBag = new OrangeBag2Adapter(appleBag);
        orangeBag.pack();*/

        //策略模式
       /* StrategyClient client = new StrategyClient(new RedPen());
        client.executeDraw(10,1,1);*/

       //观察者模式
        /*Subject subject =new Subject();
        new BinaryObserver(subject);
        new HexObserver(subject);
        subject.setState(11);*/

        //模版模式
        ConcreteTemplate template = new ConcreteTemplate();
        template.templateMethod();
    }
}