package com.hxh.design_pattern.bean.bag;

import android.util.Log;

import com.hxh.design_pattern.create.factory.factorymethod.Bag;

public class OrangeBag1 implements Bag {
    public OrangeBag1() {
        pack();
    }

    @Override
    public void pack() {
        Log.d("打包类型", "橘子打包");
    }
}
