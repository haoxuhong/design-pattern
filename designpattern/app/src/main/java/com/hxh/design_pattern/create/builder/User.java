package com.hxh.design_pattern.create.builder;

public class User {
    private String name;
    private String nickName;
    private String sex;
    private int age;

    public User(String name, String nickName, String sex, int age) {
        this.name = name;
        this.nickName = nickName;
        this.sex = sex;
        this.age = age;
    }

    public static UserBuilder builder(){
        return new UserBuilder();
    }
}
