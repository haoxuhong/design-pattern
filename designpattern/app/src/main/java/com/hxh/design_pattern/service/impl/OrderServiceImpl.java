package com.hxh.design_pattern.service.impl;


import android.util.Log;

import com.hxh.design_pattern.service.OrderService;

/**
 * 本地订单
 */
public class OrderServiceImpl implements OrderService {
    @Override
    public int saveOrder() {
        Log.d("保存订单","下单成功，订单号： 888888");

        return 888888;
    }
}
