package com.hxh.design_pattern.create.factory.factorymethod;

import com.hxh.design_pattern.bean.bag.AppleBag1;
import com.hxh.design_pattern.bean.bag.AppleBag2;

public class AppleBagFactory implements BagFactory {
    @Override
    public Bag getBag(String type) {

        if ("A".equals(type)) {
            return new AppleBag1();
        } else {
            if ("B".equals(type)) {
                return new AppleBag2();
            } else {
                return null;

            }
        }
    }
}
