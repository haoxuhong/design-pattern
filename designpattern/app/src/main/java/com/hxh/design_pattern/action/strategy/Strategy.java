package com.hxh.design_pattern.action.strategy;

public interface Strategy {
   void draw(int radius,int x,int y);
}
