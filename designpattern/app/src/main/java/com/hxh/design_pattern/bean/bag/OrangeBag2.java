package com.hxh.design_pattern.bean.bag;

import android.util.Log;

import com.hxh.design_pattern.create.factory.factorymethod.Bag;

public class OrangeBag2 implements Bag {
    public OrangeBag2() {
        pack();
    }


    @Override
    public void pack() {
        Log.d("打包类型", "橘子打包2");
    }
}
