package com.hxh.design_pattern.action.template;

import android.util.Log;

//模版模式
public abstract class Template {
    public void templateMethod() {
        init();
        apply();
        end();
    }

    protected void end() {
        Log.d("end", "end抽象层已经实现了，子类也可以选择覆写");

    }

    protected abstract void apply();

    protected void init() {
        Log.d("init", "init抽象层已经实现了，子类也可以选择覆写");
    }

    ;
}
