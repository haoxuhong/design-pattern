package com.hxh.design_pattern.create.factory.factorymethod;

public interface Bag {
    void pack();
}
