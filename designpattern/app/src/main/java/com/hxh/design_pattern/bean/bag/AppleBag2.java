package com.hxh.design_pattern.bean.bag;

import android.util.Log;

import com.hxh.design_pattern.create.factory.factorymethod.Bag;

public class AppleBag2 implements Bag {
    public AppleBag2() {
        pack();
    }

    @Override
    public void pack() {
        Log.d("打包类型", "苹果打包2");
    }
}
