package com.hxh.design_pattern.create.factory.factorymethod;

import com.hxh.design_pattern.bean.bag.OrangeBag1;
import com.hxh.design_pattern.bean.bag.OrangeBag2;

public class OrangeBagFactory implements BagFactory {
    @Override
    public Bag getBag(String type) {

        if ("A".equals(type)) {
            return new OrangeBag1();
        } else {
            if ("B".equals(type)) {
                return new OrangeBag2();
            } else {
                return null;

            }
        }
    }
}
