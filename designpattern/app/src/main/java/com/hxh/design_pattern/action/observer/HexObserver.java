package com.hxh.design_pattern.action.observer;

import android.util.Log;

public class HexObserver extends Observer {
    public HexObserver(Subject subject) {

        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        String result = Integer.toHexString(subject.getState()).toUpperCase();
        Log.d("订阅的数据发生变化", "新的数据处理为十六进制值为：" + result);

    }
}
