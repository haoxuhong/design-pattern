package com.hxh.design_pattern.bean.bag;

import android.util.Log;

import com.hxh.design_pattern.create.factory.factorymethod.Bag;

public class AppleBag1 implements Bag {
    public AppleBag1() {
        pack();
    }

    @Override
    public void pack() {
        Log.d("打包类型", "苹果打包1");
    }
}
