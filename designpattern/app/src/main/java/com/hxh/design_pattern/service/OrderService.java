package com.hxh.design_pattern.service;

/**
 * 订单接口
 */
public interface OrderService {
    int saveOrder();

}
