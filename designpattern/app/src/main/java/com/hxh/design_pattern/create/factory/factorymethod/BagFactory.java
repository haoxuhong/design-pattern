package com.hxh.design_pattern.create.factory.factorymethod;

public interface BagFactory {
     Bag getBag(String type);//打包指令
}
