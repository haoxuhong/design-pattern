package com.hxh.design_pattern.action.strategy;

public class StrategyClient {
    private Strategy strategy;

    public StrategyClient(Strategy strategy) {
        this.strategy = strategy;
    }

    public void executeDraw(int radius,int x,int y){

         strategy.draw(radius,x,y);
    }
}
