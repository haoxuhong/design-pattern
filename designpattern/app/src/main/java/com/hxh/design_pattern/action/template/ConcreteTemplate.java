package com.hxh.design_pattern.action.template;

import android.util.Log;

public class ConcreteTemplate extends Template {
    @Override
    protected void apply() {
        Log.d("apply", "子类实现了抽象方法apply");

    }

    @Override
    protected void end() {
//        super.end();
        Log.d("end", "子类实现了抽象方法end");

    }
}
