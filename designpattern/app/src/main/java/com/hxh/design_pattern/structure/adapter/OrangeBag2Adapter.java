package com.hxh.design_pattern.structure.adapter;

import com.hxh.design_pattern.bean.bag.AppleBag2;
import com.hxh.design_pattern.bean.bag.OrangeBag2;

public class OrangeBag2Adapter extends OrangeBag2 {
    private AppleBag2 appleBag2;

    public OrangeBag2Adapter(AppleBag2 appleBag2) {
        this.appleBag2 = appleBag2;
    }

    @Override
    public void pack() {
        if (appleBag2 != null)
            appleBag2.pack();
    }
}
